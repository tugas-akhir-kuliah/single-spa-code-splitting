class UserPanel extends HTMLElement {
  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: "open" });
    const channel = new BroadcastChannel("currentUser");
    channel.onmessage = (e) => {
      this.update(shadowRoot, e.data);
    };
  }

  connectedCallback() {
    this.update(this.shadowRoot);
  }

  update(shadowRoot, username) {
    shadowRoot.innerHTML = this.getHtml(username);
    if (username) {
      const logoutButton = shadowRoot.querySelector("#logout-button");
      logoutButton.addEventListener("click", () => this.onLogout());
    } else {
      const loginButton = shadowRoot.querySelector("#login-button");
      loginButton.addEventListener("click", () => this.onLogin());
    }
  }

  onLogin() {
    window.history.pushState(null, null, "/authentication/login");
  }

  onLogout() {
    const channel = new BroadcastChannel("authentication");
    channel.postMessage("logout");
    window.history.pushState(null, null, "/");
  }

  getHtml(username) {
    let html = `
          <link crossorigin="anonymous" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" rel="stylesheet">
          <style>
            #user-panel-container:hover {
              padding: 1em;
              margin: 1em;
              border: 3px solid #b51588;
              width: 400px;
            }
                        
            #user-panel-container:not(:empty):hover::after {
              content: "Team Authentication - UserPanel (VanilljaJS)";
              background: white;
              position: absolute;
              margin-top: 5px;
              padding: 5px;
              color: #b51588;
            }          
            .userInfo {
              border-right: 1px solid;
              padding-right: 1em;
              color: rgba(255,255,255,.55)
            }
            .user-panel {
              display: flex;
              flex-direction: row;
            }
            
            #logout-button, #login-button {
              cursor: pointer;
              color: rgba(255,255,255,.55);
              margin-left: 1em;
            }
          </style>
          <div id="user-panel-container">
      `;
    if (username) {
      html += `
          <div class="user-panel">
              <span class="navbar-text userInfo">${username}</span>
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a id="logout-button" aria-current="page" class="nav-link">Logout</a>
                </li>
              </ul>
            </div>
      `;
    } else {
      html += `
          <ul class="navbar-nav">
            <li class="nav-item">
              <a aria-current="page" class="nav-link" id="login-button">Login</a>
            </li>
          </ul>
        `;
    }
    html += "</div>";
    return html;
  }

  disconnectedCallback() {
    const loginButton = this.shadowRoot.querySelector("#login-button");
    const logoutButton = this.shadowRoot.querySelector("#logout-button");
    if (loginButton) {
      loginButton.removeEventListener("click");
    }
    if (logoutButton) {
      logoutButton.removeEventListener("click");
    }
  }
}

window.customElements.define("authentication-user-panel", UserPanel);
